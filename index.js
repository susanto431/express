const express = require('express');
const app = express();

const customerRouter = require('./router/customerRouter')
const productRouter = require('./router/productRouter')

app.use(express.json())

app.use(customerRouter)
app.use(productRouter)

app.listen(3000, () => {
    console.log(`Server started on 3000`);
});