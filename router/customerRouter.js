const express = require('express');
const route = express.Router();


let dataCustomer = [
    {
        id: 1,
        nama: "Susanto",
        alamat: "Bekasi"
    },
    {
        id: 2,
        nama: "Kartika",
        alamat: "Bekasi"
    },
    {
        id: 3,
        nama: "Dezan",
        alamat: "Bekasi"
    }
]
// Object => Customer

// Butuh API yg bisa request utk membuat / create sebuah produk
route.post('/api/customer', (req, res) => {
    const { nama, alamat } = req.body
    if (nama == undefined || alamat == undefined) {
        res.status(400).json("Bad Request because some data is not sent")
        return
    }

    const nextId = dataCustomer.length + 1

    dataCustomer.push({
        id: nextId,
        nama: nama,
        alamat: alamat
    })

    res.status(200).json(dataCustomer)

});
// Butuh API yg bisa request utk melihat semua data produk
route.get('/api/customer', (req, res) => {
    const customer = req.params.customer
    res.status(200).json(dataCustomer)
});


route.get('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const filteredCustomer = dataCustomer.find(i => i.id == idCustomer)
    console.log(filteredCustomer)
    // validasi pencarian data
    if (filteredCustomer === undefined) {
        res.status(404).json("Data Customer is Not Found")
        return
    }

    res.status(200).json(filteredCustomer)
});
// Butuh API yg bisa request utk mengubah semua data produk
route.put('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const { nama, alamat } = req.body

    if (nama === undefined || alamat === undefined) {
        res.status(400).json("Bad Request : Not Found")
        return
    }

    let isFound = false
    for (var x = 0; x < dataCustomer.length; x++) {
        if (dataCustomer[x].id == idCustomer) {
            dataCustomer[x].nama = nama
            dataCustomer[x].alamat = alamat

            isFound = true
            break
        }
    }
    if (isFound == false) {
        res.status(404).json("Data customer not found")
        return
    } else {
        res.status(200).json("Updated")
        return
    }
});
// Butuh API yg bisa request utk mendelete semua data produk
route.delete('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const searchCustomer = dataCustomer.find(x => x.id == idCustomer)
    if (searchCustomer === undefined) {
        res.status(404).json("Not Found")
        return
    }

    // return index dari variable searchCustomer
    const index = dataCustomer.indexOf(searchCustomer)

    // Argument (start) = menghapus array dari index berapa
    // argument (end) = mau menghapus sebanyak berapa data?
    dataCustomer.splice(index, 1)
    res.status(200).json("Delete")
});

// entitas / action => customer/login
route.post('/api/customer/login', (req, res) => {
    return
})

route.post('/api/customer/order', (req, res) => {
    return
})

module.exports = route