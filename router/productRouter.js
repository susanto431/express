const { retry } = require('async');
const express = require('express');
const router = express.Router();


// Object => Product
// CRUD => BASIC FUNCTION
// CREATE
// READ
// UPDATE
// DELETE

// Tugas
let dataProduct = [
    {
        id: 1,
        namaProduct: "HP Notebook 14s-dk0073au",
        namaBrand: "HP",
        ukuranLayar: 14,
        cpu: "AMD A-Series",
        sistemOperasi: "Windows 10 Home"
    },
    {
        id: 2,
        namaProduct: "Lenovo Ideapad C340",
        namaBrand: "Lenovo",
        ukuranLayar: 14,
        cpu: "Intel Core i7",
        sistemOperasi: "Windows 10 Home"
    },
    {
        id: 3,
        namaProduct: "ASUS X450C",
        namaBrand: "ASUS",
        ukuranLayar: 14,
        cpu: "Intel HM76 Chipset",
        sistemOperasi: "Windows 8"
    }
]




// Butuh API yg bisa request utk melihat semua data produk
router.get('/api/product', (req, res) => {
    const product = req.params.product
    res.status(200).json(dataProduct)
});

router.get('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const cariProduct = dataProduct.find(i => i.id == idProduct)

    if (cariProduct === undefined) {
        res.status(400).json("Data Tidak Ada")
        return
    }
    res.status(200).json(cariProduct)
});

// POST
router.post('/api/product', (req, res) => {
    const { namaProduct, namaBrand, ukuranLayar, cpu, sistemOperasi } = req.body
    if (namaProduct === undefined || namaProduct === undefined || ukuranLayar === undefined || cpu === undefined || sistemOperasi === undefined) {
        res.status(400).json("Data Tidak Valid")
        return
    }

    const nextProduct = dataProduct.length + 1;
    dataProduct.push({
        no: nextProduct,
        namaProduct: namaProduct,
        namaBrand: namaBrand,
        ukuranLayar: ukuranLayar,
        cpu: cpu,
        sistemOperasi: sistemOperasi
    })
    res.status(200).json(dataProduct)

});


// Butuh API yg bisa request utk mendelete semua data produk
router.delete('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const delProduct = dataProduct.find(i => i.id == idProduct)

    if (delProduct === undefined) {
        res.status(400).json("Tidak ada Data yang bisa di delete!")
        return
    }

    const index = dataProduct.indexOf(delProduct)
    dataProduct.splice(index, 1)
    res.status(200).json("Berhasil di delete!")
});


// Butuh API yg bisa request utk mengubah semua data produk
router.put('/api/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const { namaProduct, namaBrand, ukuranLayar, cpu, sistemOperasi } = req.body

    if (namaProduct === undefined || namaProduct === undefined || ukuranLayar === undefined || cpu === undefined || sistemOperasi === undefined) {
        res.status(400).json("Data Tidak Valid")
        return
    }

    let isFound = false
    for (var x = 0; x < dataProduct.length; x++) {
        if (dataProduct[x].id == idProduct) {
            dataProduct[x].namaProduct = namaProduct
            dataProduct[x].namaBrand = namaBrand
            dataProduct[x].ukuranLayar = ukuranLayar
            dataProduct[x].cpu = cpu
            dataProduct[x].sistemOperasi = sistemOperasi

            isFound = true
            break
        }
    }
    if (isFound == false) {
        res.status(400).json("Data tidak ditemukan")
        return
    }
    res.status(200).json("Updated")


});

module.exports = router